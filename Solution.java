/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
 
 public class Solution {
    public ListNode reverseList(ListNode head){
        ListNode current = null; 
        while(head!=null){
            ListNode next = head.next;
            head.next = current;
            current = head;
            head = next;
        }
        return current;
    }
 }